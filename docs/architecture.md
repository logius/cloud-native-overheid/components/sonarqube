
# SonarQube 

## Introduction

This document provides an overview of the architecture for SonarQube and discusses scenarios for a active/standby configuration. 

## Architecture

SonarQube webservice is the main component, supported by services from postgreSQL, sealed secrets and s3. Postgres data is replicated with help from pgBackRest, using the design pattern: https://access.crunchydata.com/documentation/postgres-operator/latest/architecture/disaster-recovery/.

The S3 service is expected to be deployed separately in K8s clusters in two Datacenters. We make no assumptions about replication between these clusters. For SonarQube, only one instance of S3 is needed.

Backups are created by pgBackRest in two locations: on a K8s volume in AZ1, and in a S3 buckets on AZ2. In this way the data is always available in two AZs.

```mermaid
flowchart TD;

  subgraph AZ2 ["AZ2 (standby)"]

    subgraph StorageAZ2 [Storage]
      BackupVaultVolumeAZ2
    end     

    BackupVaultAZ2(BackupVault)
    BackupVaultAZ2--> BackupVaultVolumeAZ2(BackupVault Volume)
  end

  subgraph AZ1 ["AZ1 (active)"]

    subgraph StorageAZ1 [Storage]
      PostgresVolumeAZ1
      PostgresBackupVolumeAZ1(Postgres BackupVolume)
    end     
    
    SonarQubeAZ1(SonarQube 1x)
    PostgresAZ1(Postgres 2x)
    SonarQubeAZ1 --SQL--> PostgresAZ1
    PostgresAZ1 --> PostgresVolumeAZ1(Postgres  Volume 2x)
    
    PostgresAZ1 --> replication--> BackupVaultAZ2 & PostgresBackupVolumeAZ1
 
  end 
```

Ansible role for SonarQube creates sealed secrets for postgres and SonarQube admin in s3 bucket. The default installation will use the S3 in the same K8s cluster. For production is prefered to save the sealed secrets in the standby site as shown below. The reason is that these secrets should be available to the standby site in case of failover. 

```mermaid
flowchart TD;

  subgraph AZ2 ["AZ2 (standby)"]

    BackupVaultAZ2(BackupVault)
  end

  subgraph DeployerTool ["Platform-runner"]
    AnsibleRole(SonarQube Ansible role) -- sealedsecrets --> BackupVaultAZ2
  end 
```

Promoting the standby is shown in the next diagram. The failover code will read the sealed secrets from the bucket, install these in the SonarQube namespace and proceeds with installing postgres as a standby, promote it and finally install SonarQube.

```mermaid
flowchart TD;

  subgraph AZ2 ["AZ2 (standby --> active)"]

    subgraph StorageAZ2 [Storage]
      PostgresVolumeAZ2
      PostgresBackupVolumeAZ2(Postgres BackupVolume)
      BackupVaultVolumeAZ2
    end     

    BackupVaultAZ2(BackupVault)
    BackupVaultAZ2--> BackupVaultVolumeAZ2(BackupVault Volume)
    
    SonarQubeAZ2(SonarQube 1x)
    PostgresAZ2(Postgres 2x)
    SonarQubeAZ2 --SQL--> PostgresAZ2
    PostgresAZ2 --> PostgresVolumeAZ2(Postgres  Volume 2x)
    
    PostgresAZ2 --> replication--> BackupVaultAZ2 & PostgresBackupVolumeAZ2
 
  end

  subgraph AZ1 ["AZ1 (down)"]
    down
  end 
```
